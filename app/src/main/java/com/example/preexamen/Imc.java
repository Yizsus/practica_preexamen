package com.example.preexamen;

import java.io.Serializable;

public class Imc implements Serializable {
    private int AlturaMetro;
    private float AlturaCentimetro;
    private float Peso;

    public Imc(int AlturaMetro, float alturacentimetro, float peso) {
        this.AlturaMetro = AlturaMetro;
        this.AlturaCentimetro = AlturaCentimetro;
        this.Peso = Peso;
    }

    public Imc(){

    }

    public int getAlturaMetro() {
        return AlturaMetro;
    }

    public float getAlturaCentimetro() {
        return AlturaCentimetro;
    }

    public float getPeso() {
        return Peso;
    }

    public void setAlturaMetro(int AlturaMetro) {
        this.AlturaMetro = AlturaMetro;
    }

    public void setAlturaCentimetro(float AlturaCentimetro) {
        this.AlturaCentimetro = AlturaCentimetro;
    }

    public void setPeso(float Peso) {
        this.Peso = Peso;
    }

    public float calcularimc (){
        return (this.Peso/((this.AlturaMetro+(this.AlturaCentimetro/100)) * (this.AlturaMetro+(this.AlturaCentimetro/100)) ));
        //return this.alturacentimetro/100;
    }
}
