package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnImc;
    private Button btnConvertir;
    private Button btnTerminar;
    private Imc Imc;
    private Conversiones Conversion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        btnImc = (Button) findViewById(R.id.btnImc);
        btnConvertir = (Button) findViewById(R.id.btnConvertir);
        btnTerminar = (Button) findViewById(R.id.btnTerminar);

        Imc = new Imc();
        Conversion = new Conversiones();

        btnImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falto Capturar Nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, IMC_Activity.class);
                    //Enviar un dato String
                    intent.putExtra("nombre",nombre);

                    //Enviar un objeto

                    Bundle objeto = new Bundle();
                    objeto.putSerializable("imc",Imc);
                    intent.putExtras(objeto);
                    startActivity(intent);
                }
            }

        });
        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falto Capturar Nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, Conversiones_Activity.class);

                    intent.putExtra("cliente", nombre);

                    Bundle objeto = new Bundle();

                    objeto.putSerializable("conversion", Conversion);

                    intent.putExtras(objeto);

                    startActivity(intent);
                }
            }
        });
        btnTerminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
