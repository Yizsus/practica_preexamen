package com.example.preexamen;

import java.io.Serializable;

public class Conversiones implements Serializable {
    private float Cantidad;

    public Conversiones(float Cantidad) {
        this.Cantidad = Cantidad;
    }

    public Conversiones() {
    }

    public float getCantidad() {
        return Cantidad;
    }

    public void setCantidad(float Cantidad) {
        this.Cantidad = Cantidad;
    }

    public float obtenergradoscelcius(){
        return (((this.Cantidad-32)*5/9));
    }

    public float obtenergradosfaren(){
        return ((this.Cantidad * 9/5)+32);
    }

}
