package com.example.preexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class IMC_Activity extends AppCompatActivity {
    private Imc IMC;
    private TextView lblNombre;
    private EditText txtMetros;
    private EditText txtCentimetros;
    private  EditText txtKilos;
    private TextView lblResultado;
    private  TextView lblValor;
    private Button btnCalcular;
    private Button btnLimpiar;
    private  Button btnRegresar;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtMetros = (EditText) findViewById(R.id.txtMetros);
        txtCentimetros = (EditText) findViewById(R.id.txtCentimetros);
        txtKilos = (EditText) findViewById(R.id.txtPeso);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
        lblValor = (TextView) findViewById(R.id.lblValor);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnlimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();

            lblNombre.setText("Nombre: " + (datos.getString("cliente")));


        IMC = (Imc) datos.getSerializable("imc");

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtMetros.getText().toString().matches("") || txtCentimetros.getText().toString().matches("") || txtKilos.getText().toString().matches("")){
                    Toast.makeText(IMC_Activity.this,"Faltan algunos campos", Toast.LENGTH_SHORT).show();
                }else {
                    IMC.setAlturaMetro(Integer.valueOf(txtMetros.getText().toString()));
                    IMC.setAlturaCentimetro(Float.valueOf(txtCentimetros.getText().toString()));
                    IMC.setPeso(Float.valueOf(txtKilos.getText().toString()));

                    lblResultado.setText("Resultado: " + String.valueOf(IMC.calcularimc()) +" "+" Kg/m2");
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMetros.setText("");
                txtCentimetros.setText("");
                txtKilos.setText("");
                lblValor.setText("");
                lblResultado.setText("");
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
