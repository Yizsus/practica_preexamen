package com.example.preexamen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Conversiones_Activity extends AppCompatActivity {
    private TextView lblNombre;
    private TextView lblCantidad;
    private Conversiones Conversion;
    private EditText txtCantidad;
    private RadioButton rdCelsius;
    private RadioButton rdFaren;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        rdCelsius = (RadioButton) findViewById(R.id.rdCelsius);
        rdFaren = (RadioButton) findViewById(R.id.rdFaren);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar= (Button) findViewById(R.id.btnRegresar);
        lblCantidad = (TextView) findViewById(R.id.lblCantidad);

        Bundle datos = getIntent().getExtras();

        lblNombre.setText("Nombre: " + (datos.getString("cliente")));
        Conversion = (Conversiones) datos.getSerializable("conversion");

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCantidad.getText().toString().matches("")){
                    Toast.makeText(Conversiones_Activity.this, "Favor de llenar campos", Toast.LENGTH_SHORT).show();
                }else{
                    Conversion.setCantidad(Float.valueOf(txtCantidad.getText().toString()));

                    if(rdCelsius.isChecked()){
                        //Farenheit a celsius
                        lblCantidad.setText("Resultado: " + String.valueOf(Conversion.obtenergradoscelcius()));
                    }else if(rdFaren.isChecked()) {
                        lblCantidad.setText("Resultado: " + String.valueOf(Conversion.obtenergradosfaren()));
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                lblCantidad.setText("");
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLimpiar.performClick();
                Intent intent = new Intent(Conversiones_Activity.this, MainActivity.class);

                startActivity(intent);
            }
        });

    }

}
